---
title: Apply to Invest
---

Thank you for your interest in joining Kasper Group. We are currently soliciting investors who are able to commit at least $2000 in funds. In return, we guarantee a weeekly 25% return as well as live support and updates to answer any of your questions. Please take a minute to set up an account, and we will email you for further information an Ethereum address to which you can deposit your investment.


<fieldset >
<legend>Apply</legend>
<input type='hidden' name='submitted' id='submitted' value='1'/>
<label for='name' >Name: </label>
<br>
<input type='text' name='name' id='name' maxlength="50" />
<br>
<label for='email' >Email Address:</label>
<br>
<input type='text' name='email' id='email' maxlength="50" />
<br>
<label for='username' >Username:</label>
<br>
<input type='text' name='username' id='username' maxlength="50" />
<br>
<label for='password' >Password:</label>
<br>
<input type='password' name='password' id='password' maxlength="50" />
<br><br>
<input type='submit' name='Submit' value='Submit' />

</fieldset>
</form>

