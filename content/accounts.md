---
title: Welcome
subtitle: ""
---

<form action="check_credentials.php">
      <legend>Login</legend>
      <div class="container">
        <label>Username</label>
        <br>
	<input type="text" name="uname" required>
	<br>
        <label>Password</label>
        <br>
	<input type="password" name="psw" required>
	<br><br>
        <button type="submit">Login</button>
        <label>
          <input type="checkbox" checked="checked"> Remember me
        </label>
      </div>
	<br>Forgot <a href="#">password?</a>
    </form>

