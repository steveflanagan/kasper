---
title: About Us
---
We are two cryptocurrency analysts from Minnesota currently attending Northeastern University. We specialise in analysing daily volume movements on low-cap cryptocurrencies to catch coins gaining steam before they truly take off for the day. We offer a guaranteed 25% weekly return, compounded over time, and offer 24/7 support to answer any questions our investors may have.

Please contact wills_email@gmail.com or call at XXX-XXX-XXXX for more information, and thank you for taking an interest in Kasper Group.
